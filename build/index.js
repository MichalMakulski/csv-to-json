'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _busboy = require('busboy');

var _busboy2 = _interopRequireDefault(_busboy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.use(_express2.default.static(__dirname + '/public'));

app.get('/', function (req, res) {
    return res.sendFile(__dirname + '/views/index.html');
});
app.get('/output', function (req, res) {
    return res.sendFile(__dirname + '/file.json');
});

app.post('/upload', function (req, res) {
    var busboy = new _busboy2.default({ headers: req.headers });
    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        file.on('data', function (data) {
            var fileContents = data.toString('utf8').trim().split('\r\n').map(function (item) {
                return item.trim().split(',').reduce(function (data, column, idx) {
                    data['col_' + idx] = column;
                    return data;
                }, {});
            });

            _fs2.default.writeFile(__dirname + '/file.json', JSON.stringify(fileContents, null, 2), 'utf8', function (err) {
                if (err) throw err;
            });
        });
        file.on('end', function () {
            console.log('File [' + fieldname + '] Finished');
        });
    });
    busboy.on('finish', function () {
        console.log('Done parsing form!');
        res.writeHead(303, { Connection: 'close', Location: '/output' });
        res.end();
    });
    req.pipe(busboy);
});

app.listen(3000, function () {
    return console.log('App started...');
});
//# sourceMappingURL=index.js.map