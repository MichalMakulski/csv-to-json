import express from 'express';
import fs from 'fs';
import Busboy from 'busboy';

const app = express();

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => res.sendFile(__dirname + '/views/index.html'));
app.get('/output', (req, res) => res.sendFile(__dirname + '/file.json'));

app.post('/upload', (req, res) => {
    let busboy = new Busboy({ headers: req.headers });
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        file.on('data', function(data) {
            let fileContents = data.toString('utf8')
                .trim()
                .split('\r\n')
                .map(item => {
                    return item
                        .split(',')
                        .reduce((data, column, idx) => {
                            data['col_' + idx] = column;
                            return data;
                        }, {});
                });
            
            fs.writeFile(__dirname + '/file.json', JSON.stringify(fileContents, null, 2), 'utf8', (err) => {
              if (err) throw err;
            });

        });
        file.on('end', function() {
            console.log('File [' + fieldname + '] Finished');
        });
    });
    busboy.on('finish', function() {
        console.log('Done parsing form!');
        res.writeHead(303, { Connection: 'close', Location: '/output' });
        res.end();
    });
    req.pipe(busboy);
})

app.listen(3000, () => console.log('App started...'));